MODULE LispScanner;

IMPORT SYSTEM, Files, Out;

CONST
    SourceEOF* = 0;
    ListStart* = 1;
    ListEnd* = 2;
    Unknown* = 10;

TYPE
    TokenType = INTEGER;
    TokenPtr* = POINTER TO TokenDesc;
    TokenDesc = RECORD
        Type*: TokenType;
        StartPos*: LONGINT;
        EndPos*: LONGINT;
    END;

    ScannerPtr* = POINTER TO ScannerDesc;
    ScannerDesc = RECORD
        Rider: Files.Rider;
    END;

PROCEDURE IsWhitespace(c: SYSTEM.BYTE): BOOLEAN;
BEGIN
    CASE SYSTEM.VAL(CHAR, c) OF
    | 20X: RETURN TRUE;
    ELSE RETURN FALSE;
    END;
END IsWhitespace;

PROCEDURE (s: ScannerPtr) SkipWhitespace;
VAR
    c: SYSTEM.BYTE;
BEGIN
    Files.Read(s.Rider, c);
    WHILE ~s.Rider.eof & IsWhitespace(c) DO
        Files.Read(s.Rider, c);
    END;
    IF ~s.Rider.eof THEN
        Files.Set(s.Rider, Files.Base(s.Rider), Files.Pos(s.Rider) - 1);
    END;
END SkipWhitespace;

PROCEDURE (s: ScannerPtr) NextToken* (): TokenPtr;
VAR t: TokenPtr;
    b: SYSTEM.BYTE;
    c: CHAR;
    p: LONGINT;
BEGIN
    NEW(t);

    s.SkipWhitespace;

    Files.Read(s.Rider, b);
    c := SYSTEM.VAL(CHAR, b);

    p := Files.Pos(s.Rider);

    CASE c OF
    | '(':
        t.Type := ListStart;
        t.StartPos := p;
        t.EndPos := p + 1;
    | ')':
        t.Type := ListEnd;
        t.StartPos := p;
        t.EndPos := p + 1;
    ELSE
        t.Type := Unknown;
        t.StartPos := p;
    END;

    IF s.Rider.eof THEN
        t.Type := SourceEOF;
    END;

    RETURN t;
END NextToken;

PROCEDURE NewScanner* (R: Files.Rider): ScannerPtr;
VAR
    NewScanner: ScannerPtr;
BEGIN
    NEW(NewScanner);

    NewScanner.Rider := R;

    RETURN NewScanner;
END NewScanner;

END LispScanner.
