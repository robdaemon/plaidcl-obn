MODULE Main;

  IMPORT SYSTEM, Out, Files, Args, LispScanner;

  VAR
    Filename: ARRAY 80 OF CHAR;
    InputFile: Files.File;
    R: Files.Rider;
    Scanner: LispScanner.ScannerPtr;
    T: LispScanner.TokenPtr;

BEGIN
  Out.String("Plaid Common Lisp");
  Out.Ln;

  IF Args.argc < 2 THEN
    Out.String("Specify a filename.");
    Out.Ln;
    HALT(1);
  END;

  Args.Get(1, Filename);
  InputFile := Files.Old(Filename);
  IF InputFile = NIL THEN
    Out.String("File does not exist");
    Out.Ln;
    HALT(1);
  END;

  Files.Set(R, InputFile, 0);
  Scanner := LispScanner.NewScanner(R);

  T := Scanner.NextToken();
  WHILE ~(T.Type = LispScanner.SourceEOF) DO
    Out.String("Token type ");
    Out.Int(T.Type, 0);
    Out.Ln;

    T := Scanner.NextToken();
  END;

  IF T.Type = LispScanner.SourceEOF THEN
    Out.String("EOF");
    Out.Ln;
  END;

END Main.
